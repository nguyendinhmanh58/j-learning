<?php

namespace frontend\models\base;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property integer $1
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['1'], 'required'],
            [['1'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '1' => '1',
        ];
    }
}
